![alt text](logo.png "https://www.dropbox.com/s/bs3uijkk1yfgl8a/Logo.png?dl=0")
**Table of Contents**
--

- [**Table of Contents**](#table-of-contents)
- [**About This Project**](#about-this-project)
- [**Specification Requirements**](#specification-requirements)
- [**Installation**](#installation)
- [**Project Language**](#project-language)
- [**Tools Used**](#tools-used)
- [**Change Logs**](#change-logs)
- [**Download Latest Build**](#download-latest-build)
- [**Bug Report**](#bug-report)
- [**Credits**](#credits)
- [**Tester/s**](#testers)

***

**About This Project**
----------
The goal of our project is to make a fps game akin to Doom 1 or Star Wars Dark Forces.
The game is set in a spaceship that under strange circumstances has been taken over by a strange alien race. You are an engineer and seemingly the only person alive. You traverse the ship to find where the source of the alien race comes from.
***
**Specification Requirements**
----------
**Windows**

Minimum:

| OS         	| CPU      	| RAM 	| GPU 	| Storage 	| Network            	|
|------------	|----------	|-----	|-----	|---------	|--------------------	|
| Windows 10 	| Intel i5 	| 8GB 	| UHD Graphics 620    	| 5GB     	| Broadband Internet 	|

Recommended:

| OS         	| CPU      	| RAM 	| GPU 	| Storage 	| Network            	|
|------------	|----------	|-----	|-----	|---------	|--------------------	|
| Windows 10 	| Intel i7 	| 16GB 	| NVIDIA GeForce GTX 1070    	| 5GB     	| Broadband Internet 	|


**Mac OS X**

Minimum:

| OS         	| CPU      	| RAM 	| GPU 	| Storage 	| Network            	|
|------------	|----------	|-----	|-----	|---------	|--------------------	|
| Big Sur 	| Intel i5 	| 8GB 	|  AMD Radeon R9 M370X 2 GB   	| 5GB     	| Broadband Internet 	|

Recommended:

| OS         	| CPU      	| RAM 	| GPU 	| Storage 	| Network            	|
|------------	|----------	|-----	|-----	|---------	|--------------------	|
| Big Sur 	| Intel i7 	| 16GB 	| AMD Radeon Pro 5500M 4 GB    	| 5GB     	| Broadband Internet 	|


***
**Installation**
----------
No instructions yet
***
**Project Language**
----------
English
***
**Tools Used**
----------
* Project management/Kanban/Roadmap: Jira

* Audio tool: Wwise

* Digital Audio Work Station: REAPER  

* Game engine: Unity 5  

* Multiplayer server: PHOTON 2

* 3D modelling software: Blender  

* Texture painting: Substance Painter

* Level design/UI/Gameplay UI: Photoshop

* Git repository/VCS: BitBucket

* Git GUI: Sourcetree

* Code Editor software: VS-Code

* Video-Editing: Adobe Premiere Pro 2020


* Project Final Upload: GitHub
***

**Change Logs**
----------
No available logs yet

***
**Download Latest Build**
----------
No downloads available yet
***

**Bug Report**
----------
No links available
***
**Credits**
----------
John (x19141301) - x19141301@student.ncirl.ie
* Project Manager
* Artist
* Level Designer
* 3D modelling
* Texture Painter
* Programmer

Aaron (x19404024) - x19404024@student.ncirl.ie
* Audio Director
* Sound Designer
* Composer
* 3D modelling
* Programmer

Alexis (x19358953) - x19358953@student.ncirl.ie
* Multiplayer Lead
* Multiplayer Level Designer
* Programmer
***
**Tester/s**
----------
David
