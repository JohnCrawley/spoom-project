/*
File Name: PlayerController.cs
File Type: C#
Author/s: John
Created On: 04/02/2021 at 20:36
Created By: John
Last Modified On:
Description: 
This file is the player controller class, 
this allows mouse movement for camera viewing and (W, A, S, D) keys for movement
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    /*
        [SerializeField] attribute is used for marking private fields
        as serializable so Unity can save/load those values
        Documentation for more INFO: https://docs.huihoo.com/unity/5.4/Documentation/en/ScriptReference/SerializeField.html
        ------------------------------------------------------
        Declare player traits variables such as the camera object, 
        mouse sensitivity, walk speed and character controller 
    */
    [SerializeField] private Transform playerCameraObject = null;
    [SerializeField] private float mouseSensitivity = 3.5f;
    [SerializeField] private float walkSpeed = 6f;
    [SerializeField] [Range(0.0f, 0.5f)] private float moveSmoothTime = 0.3f;
    [SerializeField] [Range(0.0f, 0.5f)] private float mouseSmoothTime = 0.03f;
    [SerializeField] private bool lockCursor = true;
    float cameraPitch = 0.0f;
    CharacterController controller = null;
    Vector2 currentDirection = Vector2.zero;
    Vector2 currentDirectionVelocity = Vector2.zero;
    Vector2 currentMouseDelta = Vector2.zero;
    Vector2 currentMouseDeltaVelocity = Vector2.zero;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        if(lockCursor)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = true;
        }   
    }
    /* 
        Update these methods each frame
    */
    void Update()
    {
        UpdateMouseLook();
        UpdateMovement();
    }

    /*
    UPDATE MOUSE LOOK
    Allow the mouse to be updated and used for the controller for the camera, moving it around
    to view the game world
    */
    void UpdateMouseLook()
    {
        Vector2 targetMouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        currentMouseDelta = Vector2.SmoothDamp(currentMouseDelta, targetMouseDelta, ref currentMouseDeltaVelocity, mouseSmoothTime);
        cameraPitch -= targetMouseDelta.y * mouseSensitivity;
        cameraPitch = Mathf.Clamp(cameraPitch, -90.0f, 90.0f);
        playerCameraObject.localEulerAngles = Vector3.right * cameraPitch;
        transform.Rotate(Vector3.up * targetMouseDelta.x * mouseSensitivity);
    }
    /*
    UPDATE MOVEMENT
    Update the player movement when the player moves and smooth out the movement by using SmoothDamp
    */
    void UpdateMovement()
    {
        Vector2 targetDirectory = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        targetDirectory.Normalize();
        currentDirection = Vector2.SmoothDamp(currentDirection, targetDirectory, ref currentDirectionVelocity, moveSmoothTime);
        Vector3 velocity = (transform.forward * targetDirectory.y + transform.right * targetDirectory.x) * walkSpeed;
        controller.Move(velocity * Time.deltaTime);
    }
}
