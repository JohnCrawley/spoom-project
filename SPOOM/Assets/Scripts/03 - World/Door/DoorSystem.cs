/*
File Name: DoorSystem.cs
File Type: C#
Author/s: John
Created On: 05/03/2021 at 23:44
Created By: John
Last Modified On:
Description: 
This script will activate doors to open or if doors are locked and if the 
player has the key to open locked doors
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSystem : MonoBehaviour
{
    /*
    INTERNAL USE ONLY: DELETE FOR SUBMISSION:
    resource
    https://youtu.be/tJiO4cvsHAo 
    */

    //Door Variables//
    [SerializeField] private Animator doorAnimator = null;

    /*  
    When the player hits the trigger volume, 
    it will trigger the animation to open the door. 
    This will continue until the player leaves the volume
    */
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
                doorAnimator.Play("Door Opening", 0, 0.0f);
                Debug.Log("Open");
        }
    }

    /*
    When the player hits the trigger volume, 
    it will trigger the animation to close the door. 
    This will continue until the player enters the volume
    */
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
                doorAnimator.Play("Door Closing", 0, 0.0f);
                 Debug.Log("Close");
        }
    }



}