using System.Diagnostics;
using System.Net.Mime;
/*
File Name: MainMenu.cs
File Type: C#
Author/s: John
Created On: 23/02/2021 at 23:16
Created By: John
Last Modified On:
Description: 
This script is used to go between the menus in the main menu
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour
{
    /*
    PLAY GAME:
    Change the scene to the mission scene selection menu
    */
    public void PlayMissionOne()
    {
        SceneManager.LoadScene("Level-1");
    }

    public void PlayMissionTwo()
    {
        SceneManager.LoadScene("Level-2");
    }




    /*
    QUIT GAME:

    */
    public void Quit()
    {
        Application.Quit();
        UnityEngine.Debug.Log("QUIT QUIT QUIT");
    } 







}
