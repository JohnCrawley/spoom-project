/*
File Name: PauseMenu.cs
File Type: C#
Author/s: John
Created On: 04/03/2021 at 22:00
Created By: John
Last Modified On:
Description: 
This will open a pause menu 
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PauseMenu : MonoBehaviour
{

public Canvas pauseCanvas = null;
private bool isPaused = false;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            UnityEngine.Debug.Log("ke");
           if (isPaused)
            {
                Resume();
                Cursor.visible = false;
            }
            else
            {
                Pause();
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }


        }
    }

     void Start()
    {
     pauseCanvas.gameObject.SetActive(false);   
    }



    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("Main-Menu");
    }
     public void Pause()
    {
        Time.timeScale = 0f;
        pauseCanvas.gameObject.SetActive(true);
       this.GetComponent<PlayerController>().enabled = false;
        isPaused = true;
    }

    public void Resume()
    {
        Time.timeScale = 1f;
        pauseCanvas.gameObject.SetActive(false);
               this.GetComponent<PlayerController>().enabled = true;
        isPaused = false;
    }

    public void Quit()
    {
        Application.Quit();
    }






}
