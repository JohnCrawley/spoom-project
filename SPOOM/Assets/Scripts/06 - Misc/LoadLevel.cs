/*
File Name: LadLevel.cs
File Type: C#
Author/s: John
Created On: 05/03/2021 at 13:33
Created By: John
Last Modified On:
Description: 
When player hits trigger volume, it will load
the player into the next level
*/
using System.Net.Mime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LoadLevel : MonoBehaviour
{
    /*  
    When the player hits the trigger enter, it
    will load the player to the next level or stage
    of the game
    */
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
               SceneManager.LoadScene("Level-2");
        }
    }

}
